export interface IAdviertisement {
    id?: string
    name: string;
    message: string;
}
