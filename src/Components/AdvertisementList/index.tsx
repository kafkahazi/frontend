import {Button, Card, CardContent, Grid, TextField, Typography} from '@mui/material';
import axios from 'axios';
import React, {useEffect, useRef, useState} from 'react';
// @ts-ignore
import SockJsClient from 'react-stomp';
import {IAdviertisement} from "./type";

const AdvertisementList = () => {
    const SOCKET_URL = 'http://localhost:31000/ws-add/';
    const [messages, setMessages] = useState<IAdviertisement[]>([])
    const inputRef = useRef<IAdviertisement>({name: '', message: ''})

    useEffect(() => {
        axios.get<any, any>('http://localhost:31000/api/kafka/advertisements').then((resp) => {
            setMessages(resp.data)
        })
    }, [])

    const onConnected = () => {
        console.log("Connected!!")
    }

    const onMessageReceived = (msg: any) => {
        console.log('New Message Received!!', msg);
        setMessages(messages.concat(msg));
    }

    const handleInputChange = (e: any) => {

        if (e.target.name === 'name' || e.target.name === 'message') {
            const name: 'name' | 'message' = e.target.name
            inputRef.current[name] = e.target.value
        }
    }

    const handleSendAdvertisement = async () => {
        await axios.post('http://localhost:31000/middleware/kafka/send', inputRef.current)
    }

    console.debug("messages", messages)
    return (
        <>
            <SockJsClient
                url={SOCKET_URL}
                topics={['/topic/group']}
                onConnect={onConnected}
                onDisconnect={console.log("Disconnected!")}
                onMessage={(msg: any) => onMessageReceived(msg)}
                debug={false}
            />
            <Grid container justifyContent={"center"} style={{marginTop: 50}} wrap={"nowrap"}>
                <Grid container justifyContent={"center"} wrap={"nowrap"}>
                    <Grid item style={{marginRight: 20}}>
                        <TextField label="Nev" name={'name'} variant="outlined" onChange={handleInputChange}/>
                    </Grid>
                    <Grid item style={{marginRight: 20}}>
                        <TextField label="Uzenet" name={'message'} variant="outlined" onChange={handleInputChange}/>
                    </Grid>
                    <Button variant={"outlined"} onClick={handleSendAdvertisement}>Kuldes</Button>
                </Grid>
            </Grid>

            <Grid container justifyContent={"center"}>
                <div style={{height: '40vh', overflowY: 'scroll', padding: 50, marginTop: 50}}>
                    {messages.length > 0 && <Grid container
                                                  justifyContent={"center"}
                                                  alignItems={'center'}
                                                  direction={'column'}
                                                 >
                        {messages.map((advertisement: any) => {
                            return (
                                <Card style={{minWidth: '40vh', marginTop: 15}}>
                                    <CardContent>
                                        <Typography variant="h5" component="div">
                                            {advertisement?.name}
                                        </Typography>
                                        <Typography variant="body2">
                                            {advertisement?.message}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            )
                        })}
                    </Grid>}
                </div>
            </Grid>

        </>
    )
}

export default AdvertisementList;
