import React from 'react';
import './App.css';
import AdvertisementList from "./Components/AdvertisementList";

function App() {
  return (
    <>
      <AdvertisementList/>
    </>
  );
}

export default App;
