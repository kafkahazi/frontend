const {createProxyMiddleware} = require('http-proxy-middleware');

const PROXY = 'http://localhost:8080';
const MIDDLEWARE = 'http://localhost:8081';

module.exports = function (app) {
    app.use(
        ['/api', "/ws-add/"],
        createProxyMiddleware({
            target: PROXY,
            changeOrigin: true,
        }),
    );
    app.use(
        ['/middleware'],
        createProxyMiddleware({
            target: MIDDLEWARE,
            changeOrigin: true,
        }),
    );
};
